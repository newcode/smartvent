<div class="grid__item item clear grid__item-active">
	<div class="item__img" style="background-image: url(images/news-1.jpg);"></div>
	<div class="item__rightItem rightItem">
		<span class="rightItem__date">2016. 10. 28</span>
		<p class="rightItem__text">2016: The year of competitive rates, high quality and speed lorem ipsum long sentance mauntis</p>
	</div>
</div>
<div class="grid__item item clear">
	<div class="item__img" style="background-image: url(images/news-2.jpg);"></div>
	<div class="item__rightItem rightItem">
		<span class="rightItem__date">2016. 10. 28</span>
		<p class="rightItem__text">Continued Expansion on Global Markets is Our Objective for 2015</p>
	</div>
</div>
<div class="grid__item item clear">
	<div class="item__img" style="background-image: url(images/news-1.jpg);"></div>
	<div class="item__rightItem rightItem">
		<span class="rightItem__date">2016. 10. 28</span>
		<p class="rightItem__text">For 10 years Baltic Media has been Taking Advantage of Opportunities in Latvia</p>
	</div>
</div>