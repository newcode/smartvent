<?php include 'include/header.php'; ?>
<?php include 'include/contactBubble.php'; ?>
<header class="header header-white">
	<div class="wrapper clear">
		<div class="header__logo">
			<a href="#">
				<svg class="forAnimation" viewBox="0 0 84.941 85">
					<path fill="#636466" d="M42.47,85c23.456,0,42.471-19.028,42.471-42.5S65.926,0,42.47,0C19.015,0,0,19.028,0,42.5S19.015,85,42.47,85L42.47,85z"/><polygon fill="#FFFFFF" points="68.503,16.447 52.935,51.777 33.196,32.025 68.503,16.447 "/><polygon fill="#F15B55" points="16.438,68.555 51.745,52.977 32.006,33.224 16.438,68.555 "/><polygon fill="#FFFFFF" points="6.497,42.5 25.964,44.151 25.964,40.851 6.497,42.5 "/><polygon fill="#FFFFFF" points="78.444,42.5 58.978,44.151 58.978,40.851 78.444,42.5 "/>
				</svg>
				<svg viewBox="0 0 306.753 40.942" class="logotext">
					<path fill="#636466" d="M21.239,26.455c-0.555-1.768-1.664-3.434-3.28-4.898c-0.807-0.758-1.917-1.817-2.825-2.726l0,0l-6.256-6.008c-1.563-1.514-1.614-2.676-1.412-3.383c0.303-1.06,1.412-1.918,2.725-2.171c1.059-0.202,3.026-0.202,4.944,2.121l4.591-3.685c-1.463-1.767-3.179-3.029-5.096-3.786c-1.816-0.656-3.733-0.808-5.549-0.404c-1.664,0.303-3.229,1.06-4.49,2.121C3.229,4.796,2.27,6.209,1.816,7.825c-0.454,1.515-0.404,3.181,0.101,4.746c0.505,1.616,1.463,3.13,2.876,4.493l4.591,4.341c0.202,0.203,2.926,2.878,4.591,4.444c1.06,1.008,2.22,2.524,1.715,4.239c-0.454,1.516-2.018,2.778-3.935,3.132c-1.109,0.252-2.321,0.15-3.431-0.305C6.962,32.411,5.7,31.401,4.64,29.989L0,33.622c1.765,2.272,3.834,3.837,6.205,4.796c1.412,0.557,2.926,0.809,4.44,0.809c0.757,0,1.513-0.051,2.27-0.203c4.086-0.807,7.366-3.684,8.425-7.32C21.845,29.989,21.794,28.17,21.239,26.455L21.239,26.455z M63.046,39.227V0.051L45.44,26.302L27.833,0.051v39.176h5.852V19.336L45.44,36.803l11.704-17.467v19.891H63.046L63.046,39.227z M184.811,1.767h-6.458l17.154,39.127l16.041-39.127h-6.354l-9.839,24.688L184.811,1.767L184.811,1.767z M222.446,7.017h16.6v-5.25h-16.649h-5.802v37.46h5.802h16.649v-5.302h-16.6v-11.61h13.671v-5.302h-13.671V7.017L222.446,7.017z M275.121,40.942V1.767h-5.904v24.031L245.709,0v39.227h5.851V15.146L275.121,40.942L275.121,40.942zM306.753,1.767h-26.488v5.25h10.292v32.21h5.904V7.017h10.292V1.767L306.753,1.767z"/><polygon fill="#F15B55" points="94.536,39.143 100.994,39.143 83.841,0.018 67.799,39.143 74.155,39.143 83.992,14.456
					94.536,39.143 "/><path fill="#F15B55" d="M121.788,25.671c2.019-0.707,3.43-1.868,4.34-2.878c1.968-2.171,3.026-5.2,3.026-8.633c0-3.483-1.058-6.512-3.026-8.683c-1.513-1.717-4.389-3.736-9.334-3.736h-6.155h-4.943v37.46h5.851V26.529h4.289l9.435,12.673h6.608L121.788,25.671L121.788,25.671z M111.546,21.28V7.043h5.248c2.421,0,4.238,0.656,5.398,1.969c1.413,1.515,1.665,3.635,1.665,5.149c0,1.464-0.252,3.584-1.665,5.099c-1.16,1.312-2.977,2.019-5.398,2.019H111.546L111.546,21.28z M160.508,1.742h-26.486v5.251h10.292v32.21h5.902V6.992h10.292V1.742L160.508,1.742z"/>
				</svg>
			</a>
		</div>
		<div class="header__rightSection rightSection clear rightSection-white">
			<nav class="rightSection__nav nav">
				<ul class="nav__navList navList">
					<li class="navList__item"><a href="news.php">Market news</a></li>
					<li class="navList__item"><a href="work.php">Our work</a></li>
					<li class="navList__item active"><a href="team.php">meet the team</a></li>
					<li class="navList__item"><a href="contact.php">contact us</a></li>
				</ul>
			</nav>
			<div class="rightSection__lang lang">
				<p class="lang__current">en</p>
				<ul class="lang__list langList">
					<li class="langList__item"><a href="#">lv</a></li>
					<li class="langList__item"><a href="#">ru</a></li>
					<li class="langList__item active"><a href="#">en</a></li>
				</ul>
			</div>
			<div class="responsiveMenu">
				<span class="top"></span>
				<span class="mid"></span>
				<span class="bot"></span>
			</div>
		</div>
	</div>
</header>

<div class="scrollContent">
	<div class="teamPage">
		<div class="teamPage__ballon teamPage__ballon-left"></div>
		<div class="teamPage__ballon teamPage__ballon-right"></div>
		<div class="teamPage__teamPageWrapper teamPageWrapper">
			<div class="teamPageWrapper__aboutText aboutText">
				<h2 class="aboutText__title">“Be yourself. The world worships the original”</h2>
				<p class="aboutText__author">Ingrid Bergman</p>
				<svg viewBox="0 0 38 27.6">
					<path d="M19,26.6C14.6,23.1,8.5,21,3.2,19.8c-0.4-0.1-1-0.4-1.4-0.3c-0.2,0-0.4-0.1-0.6-0.1c0.4,1,0.9,1.2,1.6,0.6c1.3-0.2,1.7-0.6,3-0.5c1.4,0.1,2.9,0.8,4.1,1.5c3.1,1.7,5.8,3.9,8.7,5.9c0.7,0.5,1.1-0.3,0.7-0.9c-2-2.9-6.3-5-9.4-6.6c-3.2-1.6-6.6-2.3-9.7,0c-0.2,0.2-0.3,0.4-0.2,0.7c0.3,0.7,0.9,0.9,1.6,1c1.9,0.5,3.9,0.9,5.8,1.4c3.8,1.2,7.9,2.9,11.2,5.1C19.2,27.8,19.5,27,19,26.6"/><path d="M18.4,26.3c0.3-3.8-4.4-7-6.9-9.1c-1.7-1.5-3.4-2.9-5.1-4.3c-0.6-0.5-1.2-1-1.8-1.5c-0.2-0.2-1.1-1.1-0.9-0.7c0,0.3-0.1,0.6-0.1,0.9c1.4-1,4.2,1,5.3,1.7c1.5,1.1,2.9,2.4,4.1,3.9c2.4,2.9,3.8,6,5.5,9.3c0.3,0.6,1.8,1,1.5-0.1c-1.2-4.5-4.5-9-7.9-12.1c-1.8-1.7-6.8-6.1-9.7-4.1c-0.3,0.2-0.3,0.6-0.1,0.9c0.3,0.6,1,1,1.5,1.5c1.4,1.2,2.8,2.3,4.2,3.5c3.3,2.7,8.4,5.9,9.4,10.2C17.5,26.5,18.4,26.9,18.4,26.3"/><path d="M19.4,27.1c-0.9-6.5-5.4-11.8-7.5-17.9c-0.2-0.6-0.4-1.2-0.5-1.8c-0.1-0.3-0.1-0.6-0.1-1c0-0.7,0.1-0.7,0.4,0.1c1.5,1.4,2.5,3.8,3.2,5.7c0.9,2.2,1.6,4.6,2.1,7c0.5,2.2,0.5,4.5,1.2,6.7c0.2,0.6,1.3,1.2,1.4,0.3c0.3-2.2-0.4-4.7-0.8-6.8c-0.5-2.4-1.2-4.8-2.2-7c-1-2.5-3-7.7-6.3-8c-0.3,0-0.5,0.1-0.6,0.4c-0.9,3,1.3,6.6,2.6,9.1c2.2,4.3,4.8,8.4,6,13.1C18.5,27.3,19.5,27.8,19.4,27.1"/><path d="M19.7,27.6c3.2-2.1,6.9-3.7,10.5-4.8c1.9-0.6,3.8-1.1,5.8-1.5c0.5-0.1,1.8-0.1,2-0.7c0.1-0.3-0.1-0.8-0.4-1c-2.9-2.1-5.9-2-9.1-0.6c-1.9,0.8-3.7,2-5.4,3.1c-1.5,1-3.5,2.1-4.5,3.7c-0.4,0.6,0.5,1.5,1.2,1.2c4.6-2.6,11.3-10.3,17-6.3c-0.1-0.3-0.3-0.6-0.4-1c0.1-0.4-0.6,0-0.9,0c-0.9,0.2-1.7,0.4-2.6,0.6c-2,0.5-4.1,1.2-6,2c-2.7,1.2-5.7,2.5-8,4.4C18.4,27,19.3,27.8,19.7,27.6"/><path d="M20.6,26.6c1.2-4.3,6-7.4,9.3-10.1c1.4-1.1,2.8-2.3,4.2-3.4c0.6-0.5,1.4-0.9,1.8-1.6c0.2-0.3-0.2-0.8-0.4-1c-2.9-2-7,1.3-9,3.1c-3.6,3.1-7.3,7.8-8.4,12.5c-0.2,0.6,1.1,1.7,1.5,1c1.8-3.3,3.2-6.6,5.7-9.5c1.3-1.6,2.8-3,4.5-4.2c0.9-0.7,3.8-2.7,4.9-1.9c-0.1-0.3-0.3-0.6-0.4-1c-0.3,0.6-2.1,1.7-2.8,2.3c-1.5,1.3-3,2.5-4.5,3.8c-2.7,2.3-7.6,5.6-7.3,9.6C19.6,26.5,20.4,27.1,20.6,26.6"/><path d="M19.6,27.4c1.2-4.4,3.7-8.5,5.8-12.5c1.4-2.6,3.7-6.4,2.8-9.5c-0.2-0.6-0.7-0.9-1.3-0.9c-3,0.3-5,5.8-5.9,8.1c-1.5,3.9-3.2,9.1-2.8,13.3c0.1,0.5,1.1,1.4,1.4,0.6c0.6-1.8,0.6-3.8,1-5.7c0.4-1.8,0.8-3.6,1.4-5.3c0.6-1.7,1.2-3.4,2.1-5c0.6-1.2,2-4.2,3.6-4.3c-0.4-0.3-0.9-0.6-1.3-0.9c1,3.1-2.1,7.6-3.5,10.2c-1.8,3.6-3.9,7.4-4.4,11.5C18.5,27.4,19.4,27.9,19.6,27.4"/><path d="M19.4,26.1c0.8-4.7,1.4-9.5,1.9-14.2c0.3-3.4,1-8-0.9-11.1c-0.2-0.4-1.1-1-1.5-0.5c-2.2,3-2.2,7-2.1,10.7c0.1,4.3,0.2,9.2,1.4,13.4c0.2,0.5,1.3,1.2,1.4,0.3c0.3-3.8-0.8-7.9-1-11.7C18.3,9.1,18,4.6,20.3,1.5c-0.5-0.2-1-0.3-1.5-0.5c1.9,3.1,1,8,0.7,11.3c-0.4,4.4-1.3,8.8-1.2,13.3C18.3,26,19.3,26.7,19.4,26.1"/>
				</svg>
				<p class="aboutText__text">
					Cliches off - the best, the most creative, the fastest, the cleverest...
					No, you will not find anything from that here. SmART Vent - turn to success. The turn of your body, eyes, legs, brain, hands. Success arises from action.
				</p>
			</div>
			<div class="teamGrid">
				<div class="teamGrid__tmItem tmItem">
					<div class="tmItem__imgContainer imgContainer">
						<div class="imgContainer__img" style="background-image: url(images/team-1.png);"></div>
					</div>
					<div class="tmItem__textContainer textContainer">
						<p class="textContainer__name">Adrey Tarsukov</p>
						<p class="textContainer__position">Creative manager</p>
						<p class="textContainer__text">"We are what we repeatedly do. Excellence, then, is not an act, but a habit."</p>
					</div>
				</div>
				<div class="teamGrid__tmItem tmItem">
					<div class="tmItem__imgContainer imgContainer">
						<div class="imgContainer__img" style="background-image: url(images/team-1.png);"></div>
					</div>
					<div class="tmItem__textContainer textContainer">
						<p class="textContainer__name">Adrey Tarsukov</p>
						<p class="textContainer__position">Creative manager</p>
						<p class="textContainer__text">"We are what we repeatedly do. Excellence, then, is not an act, but a habit."</p>
					</div>
				</div>
				<div class="teamGrid__tmItem tmItem">
					<div class="tmItem__imgContainer imgContainer">
						<div class="imgContainer__img" style="background-image: url(images/team-1.png);"></div>
					</div>
					<div class="tmItem__textContainer textContainer">
						<p class="textContainer__name">Adrey Tarsukov</p>
						<p class="textContainer__position">Creative manager</p>
						<p class="textContainer__text">"We are what we repeatedly do. Excellence, then, is not an act, but a habit."</p>
					</div>
				</div>
				<div class="teamGrid__tmItem tmItem">
					<div class="tmItem__imgContainer imgContainer">
						<div class="imgContainer__img" style="background-image: url(images/team-1.png);"></div>
					</div>
					<div class="tmItem__textContainer textContainer">
						<p class="textContainer__name">Adrey Tarsukov</p>
						<p class="textContainer__position">Creative manager</p>
						<p class="textContainer__text">"We are what we repeatedly do. Excellence, then, is not an act, but a habit."</p>
					</div>
				</div>
				<div class="teamGrid__tmItem tmItem">
					<div class="tmItem__imgContainer imgContainer">
						<div class="imgContainer__img" style="background-image: url(images/team-1.png);"></div>
					</div>
					<div class="tmItem__textContainer textContainer">
						<p class="textContainer__name">Adrey Tarsukov</p>
						<p class="textContainer__position">Creative manager</p>
						<p class="textContainer__text">"We are what we repeatedly do. Excellence, then, is not an act, but a habit."</p>
					</div>
				</div>
				<div class="teamGrid__tmItem tmItem">
					<div class="tmItem__imgContainer imgContainer">
						<div class="imgContainer__img" style="background-image: url(images/team-1.png);"></div>
					</div>
					<div class="tmItem__textContainer textContainer">
						<p class="textContainer__name">Adrey Tarsukov</p>
						<p class="textContainer__position">Creative manager</p>
						<p class="textContainer__text">"We are what we repeatedly do. Excellence, then, is not an act, but a habit."</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>