<?php include 'include/header.php'; ?>
<?php include 'include/contactBubble.php'; ?>

<header class="header header-white">
	<div class="wrapper clear">
		<div class="header__logo">
			<a href="#">
				<svg class="forAnimation" viewBox="0 0 84.941 85">
					<path fill="#636466" d="M42.47,85c23.456,0,42.471-19.028,42.471-42.5S65.926,0,42.47,0C19.015,0,0,19.028,0,42.5S19.015,85,42.47,85L42.47,85z"/><polygon fill="#FFFFFF" points="68.503,16.447 52.935,51.777 33.196,32.025 68.503,16.447 "/><polygon fill="#F15B55" points="16.438,68.555 51.745,52.977 32.006,33.224 16.438,68.555 "/><polygon fill="#FFFFFF" points="6.497,42.5 25.964,44.151 25.964,40.851 6.497,42.5 "/><polygon fill="#FFFFFF" points="78.444,42.5 58.978,44.151 58.978,40.851 78.444,42.5 "/>
				</svg>
				<svg viewBox="0 0 306.753 40.942" class="logotext">
					<path fill="#636466" d="M21.239,26.455c-0.555-1.768-1.664-3.434-3.28-4.898c-0.807-0.758-1.917-1.817-2.825-2.726l0,0l-6.256-6.008c-1.563-1.514-1.614-2.676-1.412-3.383c0.303-1.06,1.412-1.918,2.725-2.171c1.059-0.202,3.026-0.202,4.944,2.121l4.591-3.685c-1.463-1.767-3.179-3.029-5.096-3.786c-1.816-0.656-3.733-0.808-5.549-0.404c-1.664,0.303-3.229,1.06-4.49,2.121C3.229,4.796,2.27,6.209,1.816,7.825c-0.454,1.515-0.404,3.181,0.101,4.746c0.505,1.616,1.463,3.13,2.876,4.493l4.591,4.341c0.202,0.203,2.926,2.878,4.591,4.444c1.06,1.008,2.22,2.524,1.715,4.239c-0.454,1.516-2.018,2.778-3.935,3.132c-1.109,0.252-2.321,0.15-3.431-0.305C6.962,32.411,5.7,31.401,4.64,29.989L0,33.622c1.765,2.272,3.834,3.837,6.205,4.796c1.412,0.557,2.926,0.809,4.44,0.809c0.757,0,1.513-0.051,2.27-0.203c4.086-0.807,7.366-3.684,8.425-7.32C21.845,29.989,21.794,28.17,21.239,26.455L21.239,26.455z M63.046,39.227V0.051L45.44,26.302L27.833,0.051v39.176h5.852V19.336L45.44,36.803l11.704-17.467v19.891H63.046L63.046,39.227z M184.811,1.767h-6.458l17.154,39.127l16.041-39.127h-6.354l-9.839,24.688L184.811,1.767L184.811,1.767z M222.446,7.017h16.6v-5.25h-16.649h-5.802v37.46h5.802h16.649v-5.302h-16.6v-11.61h13.671v-5.302h-13.671V7.017L222.446,7.017z M275.121,40.942V1.767h-5.904v24.031L245.709,0v39.227h5.851V15.146L275.121,40.942L275.121,40.942zM306.753,1.767h-26.488v5.25h10.292v32.21h5.904V7.017h10.292V1.767L306.753,1.767z"/><polygon fill="#F15B55" points="94.536,39.143 100.994,39.143 83.841,0.018 67.799,39.143 74.155,39.143 83.992,14.456
					94.536,39.143 "/><path fill="#F15B55" d="M121.788,25.671c2.019-0.707,3.43-1.868,4.34-2.878c1.968-2.171,3.026-5.2,3.026-8.633c0-3.483-1.058-6.512-3.026-8.683c-1.513-1.717-4.389-3.736-9.334-3.736h-6.155h-4.943v37.46h5.851V26.529h4.289l9.435,12.673h6.608L121.788,25.671L121.788,25.671z M111.546,21.28V7.043h5.248c2.421,0,4.238,0.656,5.398,1.969c1.413,1.515,1.665,3.635,1.665,5.149c0,1.464-0.252,3.584-1.665,5.099c-1.16,1.312-2.977,2.019-5.398,2.019H111.546L111.546,21.28z M160.508,1.742h-26.486v5.251h10.292v32.21h5.902V6.992h10.292V1.742L160.508,1.742z"/>
				</svg>
			</a>
		</div>
		<div class="header__rightSection rightSection clear rightSection-white">
			<nav class="rightSection__nav nav">
				<ul class="nav__navList navList">
					<li class="navList__item"><a href="news.php">Market news</a></li>
					<li class="navList__item"><a href="work.php">Our work</a></li>
					<li class="navList__item"><a href="team.php">meet the team</a></li>
					<li class="navList__item active"><a href="contact.php">contact us</a></li>
				</ul>
			</nav>
			<div class="rightSection__lang lang">
				<p class="lang__current">en</p>
				<ul class="lang__list langList">
					<li class="langList__item"><a href="#">lv</a></li>
					<li class="langList__item"><a href="#">ru</a></li>
					<li class="langList__item active"><a href="#">en</a></li>
				</ul>
			</div>
			<div class="responsiveMenu">
				<span class="top"></span>
				<span class="mid"></span>
				<span class="bot"></span>
			</div>
		</div>
	</div>
</header>

<div class="scrollContent">
	<div class="contactPage clear">
		<div class="contactPage__contactLeftSection contactLeftSection">
			<div id="map"></div>
			<div class="pageWrapper clear">
				<div class="contactLeftSection__contInfo info">
					<div class="info__infoRow infoRow clear">
						<p class="infoRow__title">Address:</p>
						<p class="infoRow__text">Aristida Briāna iela 9a Rīga, LV-1001, Latvia</p>
					</div>
					<div class="info__infoRow infoRow clear">
						<p class="infoRow__title">Tell:</p>
						<p class="infoRow__text">+371 67 283 016</p>
					</div>
					<div class="info__infoRow infoRow clear">
						<p class="infoRow__title">Email:</p>
						<p class="infoRow__text infoRow__text-red">info@smartvent.com</p>
					</div>
				</div>
				<form action="#" type="post" class="form">
					<div class="form-message"></div>
					<input name="name" type="text" class="input name" placeholder="Name">
					<input name="email" type="text" class="input email" placeholder="Email">
					<input name="telephone" type="text" class="input telephone" placeholder="Telephone">
					<textarea name="question" class="input quastion" rows="1" placeholder="Your question"></textarea>
				</form>
				<input class="submit" type="submit" name="submit" value="send" onclick="valid();">
			</div>
		</div>
	</div>
</div>

<?php include 'include/footer.php'; ?>