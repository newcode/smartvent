<div id="wrapper">
	<div class="scroll">
		<div class="rightSection__img" style="background-image: url(images/news-1.jpg);"></div>
		<div class="rightSection__img" style="background-image: url(images/news-1.jpg);"></div>
	</div>
	<div class="rightSection__arrow rightSection__arrow-left arrow">
		<div class="arrow__spike"></div>
	</div>
	<div class="rightSection__arrow rightSection__arrow-right arrow">
		<div class="arrow__spike arrow__spike-right"></div>
	</div>
</div>
<div class="rightSection__pageWrapper pageWrapper rightSectionWrapper">
	<span class="pageWrapper__date">2016. 10. 28</span>
	<h2 class="pageWrapper__articleTitle">2016 — The year of competitive rates and high quality</h2>
	<p class="pageWrapper__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
</div>